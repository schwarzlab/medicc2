# Version 1

## Version 1.1.2

* Exchange `@cache` for `@lru_cache(maxsize=None)`

## Version 1.1.1

* Hotfix: Added extra compilation arguments to fix installation bugs

## Version 1.1.0

* Faster pairwise MED distance calculation 

## Version 1.0.4

* added legacy_loh_asymm_fst
* Bugfixes
    * plot_cn_heatmap in case max cn is 1
    * Bugfix for inputs with a single chromosome in _read_tsv_as_dataframe
* Misc
    * Additional checks for import_tree
    * Some changes to prevent pandas warnings

## Version 1.0.3

* Added CHANGELOG
* Notebooks
    * Bugfix in phasing_demo
    * Added "Detecting multiple WGDs" to WGD_detection notebook
* Added --no-plot-tree flag
* Misc. warnings and f-string formats

